package migonnadrift.networkninjas.cz.migonnadrift;


import android.Manifest;
import android.content.Context;

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;

import android.location.LocationListener;
import android.location.LocationManager;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;


public class MainActivity extends AppCompatActivity implements SensorEventListener, FrictionCoefficient, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, com.google.android.gms.location.LocationListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private final float alpha = (float) 0.8;
    float[] gravity = new float[3];
    float[] linear_acceleration = new float[3];

    protected static float frictionForce;
    float frictionCoefficient;

    CanvasSurfaceView canvasSurfaceView;

    protected static final String BASE_FILENAME = "base_file.txt";
    protected static final String OUTPUT_FILENAME = "output_file";
    private static final String MAP_OUTPUT_FILENAME = "map_file";

    protected static final String MAP_OUTPUT_SEPARATOR = "_";
    protected static final String OUTPUT_FILE_SEPARATOR = "_";

    protected static final String BASE_FILE_FILENAME_SEPARATOR = ";";
    protected static final String OUTPUT_FILE_DATA_SEPARATOR = ";";
    protected static final String MAP_OUTPUT_FILE_DATA_SEPARATOR = ";";

    // https://tools.ietf.org/html/rfc4180 .csv file
    protected static final String OUTPUT_FILE_EXTENSION = ".csv";
    protected static final String MAP_OUTPUT_FILE_EXTENSION = ".csv";

    File base_file, output_file, map_output_file;
    String out, map_out;

    BufferedWriter output_file_writer, base_file_writer, map_output_file_writer;
    boolean are_files_created = false;

    LocationManager locationManager;
    LocationRequest locationRequest;

    Location location;
    long lastUpdateTime;

    private GoogleApiClient googleApiClient;

    float averageYaxisForce;

    private static final int MY_PERMISSIONS_LOCATION = 7;
    private static final int REQUEST_CHECK_SETTINGS = 6;
    private boolean isPermissionsLocation = false;
    private boolean isLocationSettingsOK = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        averageYaxisForce = getIntent().getFloatExtra("average", 0.0f);

        System.out.println("AVERAGE="+averageYaxisForce);

        canvasSurfaceView = (CanvasSurfaceView) findViewById(R.id.surfaceView);


        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        // Location
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Accelerometer
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);

        this.frictionCoefficient = this.getFrictionCoefficient();
        this.frictionForce = Friction.calculateFrictionForce(this.frictionCoefficient, Gravity.coefficient);

        canvasSurfaceView.setFrictionForce(frictionForce);

        try {
            createOutputFile(getApplicationContext());
            createBaseFile(getApplicationContext());
            createMapCoordinatesOutputFile(getApplicationContext());
            // zápis souborů do base_file.
            base_file_writer.newLine();
            base_file_writer.write(out);
            base_file_writer.newLine();
            base_file_writer.write(map_out);
//          base_file_writer.write(BASE_FILE_FILENAME_SEPARATOR); // separator will be newline
            base_file_writer.flush();
            base_file_writer.close();

            // spárování souvisejících mapCoordinates <-> accelerometer DAT
            output_file_writer.write(map_out);
            output_file_writer.newLine();
            map_output_file_writer.write(out);
            map_output_file_writer.newLine();
            output_file_writer.flush();
            map_output_file_writer.flush();

            are_files_created = true;
        } catch (IOException exception) {
            are_files_created = false;
        }

    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();


    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
            return;
        }

        long currTime = System.currentTimeMillis();
        try {
            //output_file_writer.write(Float.toString(linear_acceleration[1]) + OUTPUT_FILE_DATA_SEPARATOR + currTime + OUTPUT_FILE_DATA_SEPARATOR);
            output_file_writer.write(Float.toString(event.values[1]) + OUTPUT_FILE_DATA_SEPARATOR + currTime + OUTPUT_FILE_DATA_SEPARATOR);
            output_file_writer.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (canvasSurfaceView != null && canvasSurfaceView.myDrawingThread != null) {
            float driftCoefficient = Math.abs(event.values[1] - averageYaxisForce);

            System.out.println("EV ="+event.values[1]);
            System.out.println("DK ="+driftCoefficient);


            canvasSurfaceView.setDriftCoefficient(driftCoefficient);
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //clearAppFiles();
    }

    @Override
    public float getFrictionCoefficient() {
        // TODO if time
        return Friction.GRAVEL_LOWER_BOUND;
    }

    /**
     * Creates output file for our sensor data
     */
    private void createOutputFile(Context context) throws IOException {
        out = OUTPUT_FILENAME + OUTPUT_FILE_SEPARATOR + Long.toString(System.currentTimeMillis()) + OUTPUT_FILE_EXTENSION;
        output_file = new File(context.getFilesDir(), out);
        FileWriter fileWriter = new FileWriter(output_file, false);
        output_file_writer = new BufferedWriter(fileWriter);
    }

    /**
     * Creates baseFile for storing name of our outputFiles
     */
    private void createBaseFile(Context context) throws IOException {
        base_file = new File(context.getFilesDir(), BASE_FILENAME);
        FileWriter fileWriter = new FileWriter(base_file, true);
        base_file_writer = new BufferedWriter(fileWriter);

    }

    /**
     * Creates mapCoordinatesOutputFile
     */
    private void createMapCoordinatesOutputFile(Context context) throws IOException {
        map_out = MAP_OUTPUT_FILENAME + MAP_OUTPUT_SEPARATOR + Long.toString(System.currentTimeMillis()) + MAP_OUTPUT_FILE_EXTENSION;
        map_output_file = new File(context.getFilesDir(), map_out);
        FileWriter fileWriter = new FileWriter(map_output_file, false);
        map_output_file_writer = new BufferedWriter(fileWriter);
    }

    /**
     * Deletes app files
     */
    protected static void clearAppFiles(Context context) {
        try {
            File base_file = new File(context.getFilesDir(), BASE_FILENAME);
            FileWriter fileWriter = new FileWriter(base_file, true);

            FileReader fileReader = new FileReader(base_file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String out_file;
            while ((out_file = bufferedReader.readLine()) != null) {
                File file = new File(context.getFilesDir(), out_file);
                boolean ret = file.delete();
            }

            // and finally delete base file content
            base_file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //canvasSurfaceView.myOnResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        //canvasSurfaceView.myOnPause();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            isPermissionsLocation = false;
            requestPermissions();
            return;
        }

        isPermissionsLocation = true;
        location = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);

        // write initial location to a file
        if (location != null) {
            writeLocation(location.getLatitude(),location.getLongitude(),System.currentTimeMillis());
        }

        // get location Settings and wait for result, if not appropriate, request for permission to change
        locationRequest = createLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(createLocationRequest());
        final PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                final LocationSettingsStates locationSettingsStates = locationSettingsResult.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS: {
                        isLocationSettingsOK = true;
                        break;
                    }

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED: {
                        try {
                            isLocationSettingsOK = false;
                            status.startResolutionForResult(MainActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    }

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        isLocationSettingsOK = false;
                        break;

                }

            }
        });

        // kdyžtak dodat nějaký check.... pokud bychom nechtěli pořád odchytávat updaty
        if (true) {
            startLocationUpdates();
        }


    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_LOCATION);
    }


    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions();
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, locationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    // callback z dialogu na permissions
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isPermissionsLocation = true;
                    // permission was granted
                } else {
                    isPermissionsLocation = false;
                    // permission denied
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    protected LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(2000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    // pro odchycení návratu z modifikace settings
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // callback na potvrzení / zamítnutí dialogu pro upravení settings na location updaty
            case REQUEST_CHECK_SETTINGS: {

                if (resultCode == RESULT_OK) {
                    isLocationSettingsOK = true;
                    return;
                }
                if (resultCode == RESULT_CANCELED) {
                    isLocationSettingsOK = false;
                    return;
                }

                break;
            }

        }


    }


    // implementace gms.LocationListeneru
    @Override
    public void onLocationChanged(Location location) {
        MainActivity.this.location = location;
        MainActivity.this.lastUpdateTime = System.currentTimeMillis();
        writeLocation(location.getLatitude(),location.getLongitude(),lastUpdateTime);
    }


    private void writeLocation(double latitude, double longitude, long timestamp) {
        try {
            map_output_file_writer.write(latitude + MAP_OUTPUT_FILE_DATA_SEPARATOR
                    + longitude + MAP_OUTPUT_FILE_DATA_SEPARATOR
                    + timestamp+ MAP_OUTPUT_FILE_DATA_SEPARATOR);
            map_output_file_writer.newLine();
            map_output_file_writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
