package migonnadrift.networkninjas.cz.migonnadrift;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.IBinder;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Gravity on 18-Mar-17.
 *
 *  Třída vypočítá průměrnou sílu působící na zařízení po dobu MEASUREMENT_LENGTH
 *  po y-ové ose zařízení.
 *  Tato síla se v @{@link MainActivity} používá pro odečtení výsledné síly.
 *
 * */

public class MeasurementPreparator extends Service implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    private static final String MEASUREMENT_FILENAME = "measurement.txt";
    private static File measurement_file;

    private static BufferedWriter measurement_file_writer;

    PreparerCallback preparerCallback;

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        MeasurementPreparator getService() {

            return MeasurementPreparator.this;
        }

    }


    // num of milliseconds for initial measurement
    private static long MEASUREMENT_LENGTH = 2000;

    long startTime;

    public void setCallbacks(Activity activity) {
        preparerCallback = (PreparerCallback) activity;
        try {
            createMeasurementFile(getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Accelerometer
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    interface PreparerCallback {
        void measurementPreparationOK(float average);

        void measurementPreparationFailed();
    }


    /**
     * Creates file to store y-axis gravitational force value.
     */
    private static void createMeasurementFile(Context context) throws IOException {
        measurement_file = new File(context.getFilesDir(), MEASUREMENT_FILENAME);
        FileWriter fileWriter = new FileWriter(measurement_file, false);
        measurement_file_writer = new BufferedWriter(fileWriter);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
            return;
        }
        long currTime = System.currentTimeMillis();
        long delta = currTime - startTime;

        if (delta < MEASUREMENT_LENGTH) {
            try {
                measurement_file_writer.newLine();
                String num = Float.toString(event.values[1]);

                measurement_file_writer.write(num);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // we have enough data
        else {
            try {
                measurement_file_writer.flush();
                measurement_file_writer.close();
                mSensorManager.unregisterListener(this, mAccelerometer);
                float average = calculateAverage(measurement_file);
                preparerCallback.measurementPreparationOK(average);
            } catch (IOException e) {
                preparerCallback.measurementPreparationFailed();
                e.printStackTrace();
            }

        }


    }

    /**
     * Calculates average force on Y-axis
     */
    private static float calculateAverage(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String out_file;
        float total = 0.0f;
        int count = 0;
        float value;
        while ((out_file = bufferedReader.readLine()) != null) {
            try {
                value = Float.valueOf(out_file);
                total += value;
                count += 1;
            } catch (NumberFormatException e) {

            }
        }
        try {
            return total / count;
        } catch (ArithmeticException e) {
            return 0;
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
