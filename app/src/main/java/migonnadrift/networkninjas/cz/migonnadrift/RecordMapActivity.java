package migonnadrift.networkninjas.cz.migonnadrift;

import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.IOException;


import java.util.ArrayList;

import java.util.Date;

import java.util.LinkedList;
import java.util.ListIterator;


public class RecordMapActivity extends AppCompatActivity implements OnMapReadyCallback {
    protected LinkedList<DataPiece> dataPieceLinkedList = new LinkedList<>();
    protected ListIterator<DataPiece> iterator = dataPieceLinkedList.listIterator();

    private boolean isMap = false;
    HeatmapTileProvider mHeatmapTileProvider;
    private boolean isWeighted = true;

    String text;


    GoogleMap mMap;
    ArrayList<WeightedLatLng> mWeightedLatLngs = new ArrayList<>();
    ArrayList<LatLng> mLatLngs = new ArrayList<>();
    TileOverlay mTileOverlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_map);
        Intent intent = getIntent();
        Record record = (Record) intent.getSerializableExtra("record");
        prepareDataPieceList(record);

        setHeatMapData();

        if (isMap && mWeightedLatLngs.size() > 0) {
            setHeatMap();
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            // no mapData to be shown
            Toast.makeText(this, getString(R.string.no_map_data), Toast.LENGTH_LONG).show();
            this.finish();

        }


    }

    private void prepareDataPieceList(Record record) {


        File f = record.getOutputFile(); // output_file
        File mapFile = record.getMapFile(); // mapFile

        try {
            FileReader fileReader = new FileReader(f); // output file reader
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.readLine(); // přeskočení prvního řádku s mapFileName

            if (mapFile != null) {
                this.isMap = true;

                FileReader fileReader1 = new FileReader(mapFile); //mapFile file reader
                BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
                bufferedReader1.readLine(); // přeskočení prvního řádku s outputFileName

                String mapFileLine;
                String outputFileLine;


                while ((mapFileLine = bufferedReader1.readLine()) != null) {
                    String[] mapArr = mapFileLine.split(MainActivity.MAP_OUTPUT_FILE_DATA_SEPARATOR);

                    // should be always
                    if (mapArr.length > 2) {
                        Location location = new Location("");
                        location.setLatitude(Double.valueOf(mapArr[0]));
                        location.setLongitude(Double.valueOf(mapArr[1]));

                        Date mapDate = new Date(Long.valueOf(mapArr[2]));

                        DataPiece dataPiece = new DataPiece(location, mapDate);

                        //File file = new File(getFilesDir(), mapFileName);
                        long prevTime = 0;
                        float prevForce = 0;

                        while ((outputFileLine = bufferedReader.readLine()) != null) {
                            String[] outputArr = outputFileLine.split((MainActivity.OUTPUT_FILE_DATA_SEPARATOR));

                            // should be always
                            if (outputArr.length > 1) {

                                if (prevTime == 0) {
                                    prevTime = Long.valueOf(outputArr[1]);
                                    prevForce = Float.valueOf(outputArr[0]);
                                    continue;
                                }

                                // pokud vím že current time je větší než čas z mapy tak jsem jeden přes
                                // a rozhoduju jestli vzít curent - mapa v abs je menší než prev - mapa v abs
                                // tedy počítám delta dvou záznamů

                                Long currTime = Long.valueOf(outputArr[1]);
                                float currForce = Float.valueOf(outputArr[0]);

                                Long mapTime = dataPiece.getMapDate().getTime();
                                if (currTime > dataPiece.getMapDate().getTime()) {

                                    // calculates delta
                                    if (Math.abs(currTime - mapTime) < Math.abs(prevTime - mapTime)) {
                                        // we take currentTime
                                        dataPiece.setForce(currForce);
                                        dataPiece.setOutputDate(new Date(currTime));
                                    } else {
                                        dataPiece.setForce(prevForce);
                                        dataPiece.setOutputDate(new Date(prevTime));
                                    }

                                    iterator.add(dataPiece);
                                    break;

                                }

                                // posuneme prev na současné ukazatele
                                prevForce = currForce;
                                prevTime = currTime;


                            }

                        }
                    }
                }


            } else {
                // TODO pokud není map soubor, tak aspoň vykreslit graf závislosti síly na čase

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mMap = googleMap;

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLngs.get(0), 15));


        this.mTileOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(this.mHeatmapTileProvider));

        //printMaximum(mWeightedLatLngs);
        mHeatmapTileProvider.setOpacity(0.7);
        mTileOverlay.clearTileCache();

    }

    private void setHeatMapData() {
        iterator = dataPieceLinkedList.listIterator();

        while (iterator.hasNext()) {
            DataPiece dataPiece = iterator.next();
            LatLng latLng = new LatLng(dataPiece.getLocation().getLatitude(), dataPiece.getLocation().getLongitude());

            // non-weighted
            this.mLatLngs.add(latLng);

            // weighted
            WeightedLatLng weightedLatLng = new WeightedLatLng(latLng, dataPiece.getForce());
            mWeightedLatLngs.add(weightedLatLng);
        }
    }

    // Android HeatmapAPI neumožňuje nasetovat maxilmání hodnotu intenzity (narozdíl od .js API),
    // ale intenzita se počítá dynamicky z dataSetu, tzn. že na pól přidáme FAKEový bod
    private void addMaximumMapDataPiece() {
        LatLng latLngArcticOcean = new LatLng(83.311553, 179.976831);
        //mLatLngs.add(latLngArcticOcean);// no need to
        mWeightedLatLngs.add(new WeightedLatLng(latLngArcticOcean, 2.0f));
    }

    private void setHeatMap() {
        //addMaximumMapDataPiece();
        if (isWeighted) {
            mHeatmapTileProvider = new HeatmapTileProvider.Builder()
                    .weightedData(this.mWeightedLatLngs)
                    .build();
        } else {
            mHeatmapTileProvider = new HeatmapTileProvider.Builder()
                    .data(this.mLatLngs)
                    .build();
        }

    }


}
