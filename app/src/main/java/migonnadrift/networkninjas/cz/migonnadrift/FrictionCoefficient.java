package migonnadrift.networkninjas.cz.migonnadrift;

/**
 * Created by Gravity on 20-Feb-17.
 */

public interface FrictionCoefficient {
    float getFrictionCoefficient();
}
