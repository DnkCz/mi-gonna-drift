package migonnadrift.networkninjas.cz.migonnadrift;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class RecordsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        ListView recordsList = (ListView) findViewById(R.id.recordsList);
        RecordBuilder recordBuilder = new RecordBuilder(getApplicationContext());
        RecordsListAdapter recordsListAdapter = new RecordsListAdapter(this, getApplicationContext(), recordBuilder.getRecords());
        recordsList.setAdapter(recordsListAdapter);
    }
}
