package migonnadrift.networkninjas.cz.migonnadrift;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Gravity on 31-Mar-17.
 */

public class RecordsListAdapter extends ArrayAdapter<Record> {
    RecordsActivity recordsActivity;
    Context context;

    public RecordsListAdapter(RecordsActivity recordsActivity, Context context, ArrayList<Record> records) {
        super(context, R.layout.records_list_item_view, R.id.records_name_adapter_lbl_list_item, records);
        this.recordsActivity = recordsActivity;
        this.context = context;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);

    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            v = layoutInflater.inflate(R.layout.records_list_item_view, null);
        }

        // get row of view
        View row = super.getView(position, v, parent);
        final Record record = getItem(position);

        RelativeLayout rowWrapper = (RelativeLayout)row.findViewById(R.id.records_list_item_row_wrapper);

        TextView nameTxt = (TextView) row.findViewById(R.id.records_name_adapter_lbl_list_item);
        TextView dateOfCreateTxt = (TextView) row.findViewById(R.id.records_dateOfCreation_adapter_lbl_list_item);
        nameTxt.setText(record.getName());
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        dateOfCreateTxt.setText(df.format(record.getDateOfCreation()));

        rowWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            System.out.println("CLICKED");
                Intent intent = new Intent(getContext(), RecordMapActivity.class);
                intent.putExtra("record", record);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getContext().startActivity(intent);
            }
        });


        return row;
    }
}
