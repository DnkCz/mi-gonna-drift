package migonnadrift.networkninjas.cz.migonnadrift;

/**
 * Created by Gravity on 20-Feb-17.
 *
 * Třída obsahuje konstanty pro koeficienty tření.
 * Pro jednotlivé povrchy se uvádí různé hodnoty (interval hodnot) koeficientu tření.
 */

public class Friction {
    public static final float ICE_LOWER_BOUND = 0.1f;
    public static final float ICE_UPPER_BOUND = 0.2f;
    public static final float PAVEMENT_LOWER_BOUND = 0.3f;
    public static final float PAVEMENT_UPPER_BOUND = 0.5f;
    public static final float ASPHALT_LOWER_BOUND = 0.3f;
    public static final float ASPHALT_UPPER_BOUND = 0.6f;
    public static final float CONCRETE_LOWER_BOUND = 0.5f;
    public static final float CONCRETE_UPPER_BOUND = 0.8f;
    public static final float GRAVEL_LOWER_BOUND = 0.8f;
    public static final float GRAVEL_UPPER_BOUND = 0.9f;


    public static final float calculateFrictionForce(float frictionCoefficient, float gravitationalCoefficient) {
        return frictionCoefficient * gravitationalCoefficient;
    }
}
