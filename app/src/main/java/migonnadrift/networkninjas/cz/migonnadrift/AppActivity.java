package migonnadrift.networkninjas.cz.migonnadrift;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;


public class AppActivity extends AppCompatActivity implements MeasurementPreparator.PreparerCallback {
    private static final int APP_REQUEST_CODE = 1;

    ImageButton startButton;
    boolean isRunning = false;
    TextView messageTextView;
    Intent service;
    private boolean isBound = false;
    MeasurementPreparator measurementPreparator;

    TextDrawerThread textDrawerThread;

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MeasurementPreparator.LocalBinder localBinder = (MeasurementPreparator.LocalBinder) service;
            measurementPreparator = localBinder.getService();
            isBound = true;
            measurementPreparator.setCallbacks(AppActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app);

        startButton = (ImageButton) findViewById(R.id.startButton);
        messageTextView = (TextView) findViewById(R.id.messageTextView);


        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isRunning) {
                    isRunning = true;
                    messageTextView.setText("Running");

                    // start drawing thread
                    /*if (textDrawerThread == null) {
                        textDrawerThread = new TextDrawerThread();
                        textDrawerThread.setMessageTextView(messageTextView);
                        textDrawerThread.run();
                    }
                    */

                    service = new Intent(AppActivity.this, MeasurementPreparator.class);
                    bindService(service, serviceConnection, Context.BIND_AUTO_CREATE);
                }

            }
        });


    }


    @Override
    public void measurementPreparationOK(float average) {
        if (isBound) {
            measurementPreparator.setCallbacks(null);
            unbindService(serviceConnection);
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("average", average);
        startActivityForResult(intent, APP_REQUEST_CODE);
        /*textDrawerThread.interrupt();
        textDrawerThread = null;*/

    }

    @Override
    public void measurementPreparationFailed() {
        isRunning = false;
        /*textDrawerThread.interrupt();
        textDrawerThread = null;*/
        messageTextView.setText("Preparation not successful");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APP_REQUEST_CODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

            }
            messageTextView.setText("Click on icon to start measuring");
            isRunning = false;
        }
    }

    private class TextDrawerThread extends Thread {
        TextView messageTextView;

        public void setMessageTextView(TextView messageTextView) {
            this.messageTextView = messageTextView;
        }

        @Override
        public void run() {

            try {
                //while (!this.isInterrupted()) {
                sleep(200);
                if (this.messageTextView != null) {
                    String text = "";

                    String currText = messageTextView.getText().toString();
                    System.out.println(currText);
                    switch (currText) {

                        case "Running": {
                            text = "Running.";
                            break;
                        }
                        case "Running.": {
                            text = "Running..";
                            break;
                        }
                        case "Running..": {
                            text = "Running...";
                            break;
                        }
                        case "Running...": {
                            text = "Running";
                            break;
                        }
                    }
                    messageTextView.setText(text);
                    messageTextView.postInvalidate();
                }
                //}
            } catch (InterruptedException e) {
                e.printStackTrace();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.app_options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.records: {
                Intent records = new Intent(this, RecordsActivity.class);
                startActivity(records);
                //RecordBuilder recordBuilder = new RecordBuilder(AppActivity.this.getApplicationContext());
                //ArrayList<Record> records = recordBuilder.getRecords();
                //System.out.printn("rec=" + records.size());

                return true;
            }

            case R.id.delete_files: {
                MainActivity.clearAppFiles(getApplicationContext());
                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);

            }
        }
    }
}
