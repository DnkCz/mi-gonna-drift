package migonnadrift.networkninjas.cz.migonnadrift;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by Gravity on 31-Mar-17.
 */

public class RecordBuilder implements IRecordBuilder {
    Context context;

    public RecordBuilder(Context context) {
        this.context = context;

    }

    // získáme záznamy na základě output files, kde jsou data ze senzorů
    @Override
    public ArrayList<Record> getRecords() {
        ArrayList<Record> records = new ArrayList<>();


        File base_file = new File(context.getFilesDir(), MainActivity.BASE_FILENAME);
        try {
            FileReader fileReader = new FileReader(base_file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String out_file;

            while ((out_file = bufferedReader.readLine()) != null) {
                File file = new File(context.getFilesDir(), out_file);

                // procesujeme jenom outpu_file
                if (file.getName().startsWith(MainActivity.OUTPUT_FILENAME)) {
                    // z output file načteme první řádek a zjistíme jméno mapFile
                    FileReader fileReader1 = new FileReader(file);
                    BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
                    String mapFileName;
                    File mapFile = null;

                    if ((mapFileName = bufferedReader1.readLine()) != null) {
                        mapFile = new File(context.getFilesDir(), mapFileName);
                    }

                    // example: output_file_1122331144.csv or  map_output_11223344.csv
                    // split based on _
                    String[] nameArr = out_file.split(MainActivity.OUTPUT_FILE_SEPARATOR);
                    if (nameArr.length > 2) {
                        // split based on .
                        String[] subNameArr = nameArr[2].split("\\.");
                        if (subNameArr.length > 1) {
                            Record r = new Record(out_file, file, new Date(Long.valueOf(subNameArr[0])));
                            if (mapFile != null) r.setMapFile(mapFile);
                            records.add(r);

                        } else continue;
                    } else continue;


                }
                // nechceme načítat map_file, ten si vytáhneme z output_file
                else continue;
            }
        } catch (IOException e) {
            e.printStackTrace();

            // from new to old
            Collections.reverse(records);
            return records;

        }
        // from new to old
        Collections.reverse(records);
        return records;
    }


}
