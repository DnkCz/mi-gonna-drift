package migonnadrift.networkninjas.cz.migonnadrift;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Gravity on 20-Feb-17.
 */

public class CanvasSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    protected MyDrawingThread myDrawingThread;
    private float driftCoefficient = 0.0f;
    private float frictionForce= 0.0f;

    public CanvasSurfaceView(Context context) {
        super(context);
        this.getHolder().addCallback(this);
    }

    public CanvasSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.getHolder().addCallback(this);
    }

    public CanvasSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.getHolder().addCallback(this);
    }


    public void setDriftCoefficient(float driftCoefficient) {
        this.driftCoefficient = driftCoefficient;

    }

    public void setFrictionForce(float frictionForce) {
        this.frictionForce= frictionForce;

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (myDrawingThread == null) {
            myDrawingThread = new MyDrawingThread(holder, width, height);
            myDrawingThread.setRunning(true);
            myDrawingThread.start();

        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        myDrawingThread.setRunning(false);
        System.out.println("DESTROYED");
        while (retry) {
            try {
                myDrawingThread.join();
                retry = false;
            } catch (InterruptedException e) {
                Log.d(getClass().getSimpleName(), "Interrupted Exception", e);
            }
        }
        myDrawingThread = null;

    }


    public class MyDrawingThread extends Thread {
        private SurfaceHolder holder;
        private int width;
        private int height;
        private boolean running;



        public MyDrawingThread(SurfaceHolder holder, int width, int height) {
            this.holder = holder;
            this.width = width;
            this.height = height;
        }

        @Override
        public void run() {
            Canvas canvas = null;
            while (running) {
                try {
                    canvas = holder.lockCanvas();
                    synchronized (holder) {
                        //Log.d("drift coef",driftCoefficient+"");
                        if (canvas != null) {
                            drawScale(canvas);
                            drawForce(canvas, driftCoefficient);
                        }
                    }
                } finally {
                    if (canvas != null) {
                        holder.unlockCanvasAndPost(canvas);
                    }
                }

            }

        }

        public void setRunning(boolean running) {
            this.running = running;
        }

        /**
         * Draws basic scale on canvas
         */
        private void drawScale(Canvas canvas) {
            ShapeDrawable shapeDrawable = new ShapeDrawable();
            shapeDrawable.setShape(new RectShape());

            int[] colorArray = new int[]{Color.GREEN, Color.YELLOW, Color.RED};
            float[] positionArray = null;

            Shader shader = new LinearGradient(0, 0, width, 0, colorArray, positionArray, Shader.TileMode.MIRROR);
            shapeDrawable.getPaint().setShader(shader);
            shapeDrawable.setBounds(0, 0, width, height);
            shapeDrawable.draw(canvas);
        }

        /**
         * Draws force on canvas based on drift coefficient
         */
        public void drawForce(Canvas canvas, float driftCoefficient) {
            float force = Math.abs(driftCoefficient/frictionForce);

            if (force < 1.00) {
                ShapeDrawable shapeDrawable = new ShapeDrawable();
                shapeDrawable.setShape(new RectShape());
                shapeDrawable.getPaint().setColor(Color.BLACK);
                shapeDrawable.setBounds((int) (force * width), 0, width, height);
                shapeDrawable.draw(canvas);
            }


        }
    }


}

