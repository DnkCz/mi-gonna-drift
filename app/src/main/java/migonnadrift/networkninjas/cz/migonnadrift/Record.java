package migonnadrift.networkninjas.cz.migonnadrift;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Gravity on 31-Mar-17.
 */
public class Record implements Serializable {
    private String name; // output_file_1234567.csv
    private Date dateOfCreation; //new Date(1234567)
    private File outputFile; // File(output_file_1234567.csv)
    private File mapFile; //File(

    private boolean isMap = false;
    private boolean isAccelerometer = false;

    public Record(String name, File file, Date dateOfCreation) {
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.outputFile = file;
    }

    public void setName(String name) {
        this.name = name;

    }

    public void setDateOfCreation(Date date) {
        this.dateOfCreation = date;
    }

    public String getName() {
        return this.name;
    }

    public Date getDateOfCreation() {
        return this.dateOfCreation;
    }

    public void setFile(File file) {
        this.outputFile = file;

    }

    public File getOutputFile() {
        return this.outputFile;
    }

    public void setMapFile(File mapFile){
        this.mapFile = mapFile;

    }

    public File getMapFile(){
        return this.mapFile;
    }

}
