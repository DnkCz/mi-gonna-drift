package migonnadrift.networkninjas.cz.migonnadrift;

import android.location.Location;

import java.util.Date;

/**
 * Created by Gravity on 01-Apr-17.
 *
 * Represents Piece of data which will be used to draw data to map
 */

public class DataPiece {
    private Location location;
    private Date mapDate;
    private Date outputDate;
    private float force;


    public DataPiece(Location location, Date mapDate){
        this.location = location;
        this.mapDate = mapDate;
    }
    public DataPiece(Location location, Date mapDate, Date outputDate, float force){
        this.location = location;
        this.mapDate = mapDate;
        this.outputDate = outputDate;
        this.force = force;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setMapDate(Date mapDate) {
        this.mapDate = mapDate;
    }

    public Date getMapDate() {
        return this.mapDate;
    }

    public void setOutputDate(Date outputDate) {
        this.outputDate = outputDate;
    }

    public Date getOutputDate() {
        return this.outputDate;
    }

    public void setForce(float force){
        this.force = force;

    }

    public float getForce(){
        return this.force;
    }

}
