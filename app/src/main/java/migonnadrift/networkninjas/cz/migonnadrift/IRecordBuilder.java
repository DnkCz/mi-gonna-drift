package migonnadrift.networkninjas.cz.migonnadrift;

import java.util.ArrayList;

/**
 * Created by Gravity on 31-Mar-17.
 */

public interface IRecordBuilder {
    ArrayList<Record> getRecords();
}
